#!/usr/bin/env python
# -*- coding: utf-8 -*-

START = "Bot que lista o cardapio do RU politécnico esta funcionando, utilize /help para verificar a lista de comandos completa, sabados e domingos os cardapios são do RU central"
HELP = (
    "Lista de comandos:\n"
    "/help - Lista os comandos.\n"
    "/almoco - almoço do dia atual.\n"
    "/cafe - cafe do dia atual.\n"
    "/janta - janta do dia atual.\n"
)

